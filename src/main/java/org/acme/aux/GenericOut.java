package org.acme.aux;


import io.vertx.core.json.JsonObject;


public class GenericOut<T> {

    private Integer statusCode;
    private boolean error;
    private Object message;

    public GenericOut() {
    }

    public GenericOut(JsonObject mapResponse) {
        this.setStatusCode(mapResponse.getInteger("statusCode"));
        this.setError(mapResponse.getBoolean("error"));
        this.setMessage(mapResponse.getValue("message"));
    }


    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }
}
