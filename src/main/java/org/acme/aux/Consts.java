package org.acme.aux;

public class Consts {

    public static final String ENTITY_USERS = "users";
    public static final String ENTITY_POSTS = "posts";
    public static final String ENTITY_COMMENTS = "comments";
}
