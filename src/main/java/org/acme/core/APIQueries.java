package org.acme.core;

import io.quarkus.vertx.web.Route;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.MultiMap;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.*;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.acme.aux.GenericOut;
import org.jboss.logging.Logger;

import javax.inject.Inject;

public class APIQueries {

    private static final Logger logger = Logger.getLogger(APIQueries.class);

    private static final DeliveryOptions dops = new DeliveryOptions();

    @Inject
    EventBus bus;


    @Route(path = "/:queryType/:entity/:id", methods = Route.HttpMethod.GET)
    public void specificRecordQuery (RoutingContext rc){
        JsonObject msg = getQueryParams(rc);

        var adress = msg.getString("queryType");

        bus.request(adress, msg, dops, queryHandler(rc));
    }



    private JsonObject getQueryParams(RoutingContext rc){
        MultiMap queryParams = rc.queryParams().isEmpty() ? null : rc.queryParams();

        String queryType = rc.pathParam("queryType");
        String entity = rc.pathParam("entity");
        String id = rc.pathParam("id");

        return new JsonObject()
                .put("queryType", queryType)
                .put("entity", entity)
                .put("id", id);
    }

    private Handler<AsyncResult<Message<Object>>> queryHandler(RoutingContext rc){
        return ar -> {

            var response = ar.result() != null ? (JsonObject) ar.result().body() : new JsonObject();
            GenericOut out;
            try{

                if (ar.succeeded()) {
                    logger.warn("entre al succeeded");
                    out =  new GenericOut(response);

                } else {
                    logger.warn("Something went wrong " + ar.cause());

                    response.put("statusCode", 400);
                    response.put("error", true);
                    response.put("message", ar.cause());

                }
            } catch(Exception e) {
                response.put("statusCode", 400);
                response.put("error", true);
                response.put("message", ar.cause());
            }
            out = new GenericOut(response);
            reply(rc, out);
        };
    }

    private void reply(RoutingContext rc, GenericOut out){

        String responseString = JsonObject.mapFrom(out).encodePrettily();

        rc.response().putHeader("Content-Type", "application/json");
        rc.response().end(responseString);

    }

}



